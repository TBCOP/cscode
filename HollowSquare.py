size = int(input("What is the size of the Square?"))

for x in range(size):
    for y in range(size):
        if(x== 0 or x == size-1):
            print(" * ", end="")
        elif(y == 0 or y == size-1):
            print(" * ", end="")
        else:
            print("   ", end = "")
    print("")