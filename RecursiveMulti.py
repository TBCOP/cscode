def recurMulti(num1, num2):
    if (num1 == 0 or num2 == 0):
        return 0
    if(num1 == 1):
        return num2
    return recurMulti(num1-1, num2)+num2


print("Pick two numbers to multiply:")
num1 = int(input())
num2 = int(input())

print(recurMulti(num1, num2))